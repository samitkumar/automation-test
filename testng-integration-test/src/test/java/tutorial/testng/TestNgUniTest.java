/**
 * 
 */
package tutorial.testng;

import org.testng.annotations.Test;

/**
 * @author samit
 *
 */
public class TestNgUniTest {
	
	@Test(priority=1,description="test for test1")
	public void test1(){
		System.out.println("*****test1");
	}
	
	@Test(priority=2,description="test for test2",dependsOnMethods="test1")
	public void test2(){
		System.out.println("*****test2");
	}
}
