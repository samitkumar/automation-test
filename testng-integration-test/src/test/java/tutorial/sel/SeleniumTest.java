package tutorial.sel;

import static org.testng.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.phantomjs.PhantomJSDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class SeleniumTest {
	private WebDriver driver;
	
	@BeforeClass
	public void before(){
//		final String PHANTOM_JS_BINARY_PATH = "PANTHOM_JS_PATH";
//		String binaryPath = System.getenv(PHANTOM_JS_BINARY_PATH);
//		//set the binaries location and it should be env specific and the download can be available in phantomjs website
//		if(StringUtils.isNoneEmpty(binaryPath)){
//			fail("phantom js binary path not available : "+System.getenv(PHANTOM_JS_BINARY_PATH));
//		}
//		File f = new File(binaryPath);
//		System.setProperty("phantomjs.binary.path",f.getAbsolutePath());
	}
	
	@AfterClass
	public void after(){
		if(driver!=null){
			driver.quit();
		}		
	}
	
	@Test
	public void integrationTest() {

		driver = new PhantomJSDriver();
		driver.get("http://ec2-52-38-18-132.us-west-2.compute.amazonaws.com:8088/");
		System.out.println("****** page title name : "+driver.getTitle());
		assertEquals("index||poc", driver.getTitle());
		//other assertion should be integrate
	}
	
	public void screenShot(WebDriver driver,String fileName) throws IOException{
		TakesScreenshot ts =(TakesScreenshot)driver;
		File f = ts.getScreenshotAs(OutputType.FILE);
		FileUtils.copyFile(f, new File("."));
	}
}
